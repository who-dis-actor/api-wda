import { Injectable } from '@nestjs/common';
var elasticsearch = require('elasticsearch');

@Injectable()
export class ActorService {
    client
    constructor() {
        this.client = new elasticsearch.Client({
            host: 'https://search-db-whodisactor-sgboko5r2uj4uavuxdxrly7qry.eu-west-3.es.amazonaws.com',
            log: 'trace',
            apiVersion: '7.1', // use the same version of your Elasticsearch instance
        });
    }



    getActor() {
        return this.client.search({
            index: 'actor',
            body: {

            }
        });
    }
}

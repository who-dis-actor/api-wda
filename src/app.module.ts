import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MediaService } from './media/media.service';
import { ActorService } from './actor/actor.service';
import { MediaController } from './media/media.controller';
import { ActorController } from './actor/actor.controller';

@Module({
  imports: [],
  controllers: [AppController, MediaController, ActorController],
  providers: [AppService, MediaService, ActorService],
})
export class AppModule {}
